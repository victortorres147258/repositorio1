import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Amazontest {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/UNIPE2022/UNIPE2022/src/test/resources/firefox/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br");



    }
    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos",driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }
    @Test
    public void TestarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("shaushaus@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        Thread.sleep(3000);}

        @Test
        public void TestarLivros() throws InterruptedException {
            driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();
            Thread.sleep(3000);
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Harry Potter");
            Thread.sleep(3000);
            driver.findElement(By.id("nav-search-submit-button")).click();
            Thread.sleep(3000);
            Assert.assertEquals("Harry Potter e a pedra filosofal", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[6]/div/div/div/div/div/div[2]/div/div/div[1]/h2/a/span")).getText());
            Thread.sleep(3000);
        }
        @Test
    public void TestarNovidades() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);
        }

        @Test
    public void Testaratendimentoaocliente() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[7]")).click();
        Thread.sleep(4000);
        Assert.assertEquals("Algumas coisas que você pode fazer aqui", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[4]/h2")).getText());
        Thread.sleep(4000);
        }
        @Test
    public void TestarAbadeAmazonModa () throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[8]")).click();
        Thread.sleep(4000);
        Assert.assertEquals("Malas e Mochilas", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/div[3]/div/section/div/div[3]/div/div/div/div[6]/a")).getText());
        Thread.sleep(4000);
        }



}
